var express = require("express");
var app = express();

console.log(__dirname);
const NODE_PORT = process.env.PORT || 4000;
console.log(NODE_PORT);
app.use(express.static(__dirname + "/../client/"));

app.get("/students", (req,res)=>{
    console.log("I am in students endpoint");
    var students = [
        {
            name: "Kenneth",
            age: 25
        },
        {
            name: "Alex",
            age: 26
        }
    ]
    res.status(200).json(students);
});

app.listen(NODE_PORT, ()=>{
    console.log(`Web App started at ${NODE_PORT}`);
});