# How to commit my codes #

* git add .
* git commit -m "Message"
* git push origin master -u

# SETUP #

This is day3 setup steps. 

* git init
* git remote add origin <git URL> 
* npm init (answer all the questions, enter the entry point accordingly)
* mkdir <server directory>
* create an app.js under server directory
* create .gitignore to ignore directories/files
